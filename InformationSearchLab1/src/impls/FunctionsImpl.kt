package impls

import interfaces.Functions

/**
 * Класс, реализующий интерфейс Functions
 */
class FunctionsImpl : Functions {
    override fun calculate(a: Int, b: Int) = a + b

    override fun substringCounter(s: String, sub: String): Int {
        return s.split(sub).count() - 1
    }

    override fun splitterSorter(s: String, sub: String): List<String> {
        return s.split(sub)
    }

    override fun uniqueCounter(s: String, sub: String): Map<String, Int> {
        val resMap = mutableMapOf<String, Int>()

        for (i in s.split(sub)) {
            if (resMap.contains(i)) resMap.getValue(i).inc().also { resMap[i] = it } else {
                resMap += Pair(i, 1)
            }
        }
        return resMap
    }

    override fun isPalindrome(s: String): Boolean {
        val some = StringBuilder(s)
        val rev = some.reverse().toString()
        if (rev == s && s != "") {
            return true
        }
        return false
    }

    override fun invert(s: String): String {
        return StringBuilder(s).reverse().toString()
    }
}
